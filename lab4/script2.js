console.log('JavaScript datoteka je učitana.');

// Mijenja boju naslova
function changeTitleColor() {
    const title = document.querySelector('h1');
    title.style.color = 'blue';
}

window.onload = function() {
    changeTitleColor();
}